<?php
/**
 * @file
 * Functions for UW logos.
 */

/**
 * Page callback for page UW logos settings.
 *
 * @return array
 *   The render array.
 */
function _uw_ct_logos_settings_form($form, &$form_state) {

  // The container for the form.
  $form['uw_logos'] = array(
    '#type' => 'container',
  );

  // The message that will appear on each node page.
  $form['uw_logos']['uw_logos_node_message'] = array(
    '#type' => 'text_format',
    '#title' => 'Node message',
    '#description' => 'Enter the text that will appear on all node pages of UW logos.',
    '#default_value' => variable_get('uw_logos_node_message') ? variable_get('uw_logos_node_message')['value'] : '',
    '#format' => 'uw_tf_basic',
  );

  return system_settings_form($form);
}
