<?php

/**
 * @file
 * uw_ct_logos.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_logos_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_logos_site_footer|node|uw_ct_logos|form';
  $field_group->group_name = 'group_uw_logos_site_footer';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_logos';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Site footer',
    'weight' => '12',
    'children' => array(
      0 => 'field_uw_logos_sf_logo',
      1 => 'field_uw_logos_sf_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uw-logos-site-footer field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uw_logos_site_footer|node|uw_ct_logos|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Site footer');

  return $field_groups;
}
