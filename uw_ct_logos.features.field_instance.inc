<?php

/**
 * @file
 * uw_ct_logos.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ct_logos_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-uw_ct_logos-field_uw_logo_description'.
  $field_instances['node-uw_ct_logos-field_uw_logo_description'] = array(
    'bundle' => 'uw_ct_logos',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter the descriptive text that will appear on the page that shows this logo.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_logo_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 0,
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_standard' => 0,
          'uw_tf_standard_sidebar' => 0,
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 1,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -45,
            ),
            'plain_text' => array(
              'weight' => -41,
            ),
            'single_page_remote_events' => array(
              'weight' => -42,
            ),
            'uw_tf_basic' => array(
              'weight' => -50,
            ),
            'uw_tf_comment' => array(
              'weight' => -43,
            ),
            'uw_tf_contact' => array(
              'weight' => -44,
            ),
            'uw_tf_standard' => array(
              'weight' => -49,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -47,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -46,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -48,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-uw_ct_logos-field_uw_logos_faculty'.
  $field_instances['node-uw_ct_logos-field_uw_logos_faculty'] = array(
    'bundle' => 'uw_ct_logos',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choose the faculty for this logo.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'i18n_taxonomy',
        'settings' => array(),
        'type' => 'i18n_taxonomy_term_reference_link',
        'weight' => 2,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_logos_faculty',
    'label' => 'Faculty',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_select',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'node-uw_ct_logos-field_uw_logos_logos'.
  $field_instances['node-uw_ct_logos-field_uw_logos_logos'] = array(
    'bundle' => 'uw_ct_logos',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 3,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_logos_logos',
    'label' => 'Logos',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'button',
      'allowed_bundles' => array(
        'call_to_action' => -1,
        'eff_highlighted_fact' => -1,
        'uw_para_logo' => 'uw_para_logo',
      ),
      'bundle_weights' => array(
        'call_to_action' => 2,
        'eff_highlighted_fact' => 3,
        'uw_para_logo' => 4,
      ),
      'default_edit_mode' => 'closed',
      'entity_translation_sync' => FALSE,
      'title' => 'Logo',
      'title_multiple' => 'Logos',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'paragraphs_embed',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-uw_ct_logos-field_uw_logos_sf_link'.
  $field_instances['node-uw_ct_logos-field_uw_logos_sf_link'] = array(
    'bundle' => 'uw_ct_logos',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter the link that the site footer logo will be directed to.   This is REQUIRED if you enter a logo.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 5,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_logos_sf_link',
    'label' => 'Link',
    'required' => 0,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'entity_translation_sync' => FALSE,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_allowed_values' => '',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'link_field',
      'weight' => 11,
    ),
    'workbench_access_field' => 0,
  );

  // Exported field_instance: 'node-uw_ct_logos-field_uw_logos_sf_logo'.
  $field_instances['node-uw_ct_logos-field_uw_logos_sf_logo'] = array(
    'bundle' => 'uw_ct_logos',
    'deleted' => 0,
    'description' => 'Select the logo to be used as the site footer on WCMS sites.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 4,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_logos_sf_logo',
    'label' => 'Logo',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'alt_field_required' => 1,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => '',
      'file_extensions' => 'png jpg',
      'focal_point' => 0,
      'focal_point_preview' => 0,
      'focal_point_styles' => array(
        'image_gallery_small' => 0,
        'image_gallery_squares' => 0,
        'image_gallery_standard' => 0,
        'image_gallery_wide' => 0,
      ),
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '2 MB',
      'max_resolution' => '400x200',
      'min_resolution' => '20x20',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__banner-750w' => 0,
          'colorbox__banner-wide' => 0,
          'colorbox__body-500px-wide' => 0,
          'colorbox__field-slideshow-slide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__image_gallery_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__image_gallery_standard' => 0,
          'colorbox__image_gallery_wide' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__uw_service_icon' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_banner-wide' => 0,
          'image_body-500px-wide' => 0,
          'image_field-slideshow-slide' => 0,
          'image_focal_point_preview' => 0,
          'image_image_gallery_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_image_gallery_standard' => 0,
          'image_image_gallery_wide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_uw_service_icon' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'label_help_description' => '',
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 10,
    ),
    'workbench_access_field' => 0,
  );

  // Exported field_instance: 'node-uw_ct_logos-field_uw_logos_type'.
  $field_instances['node-uw_ct_logos-field_uw_logos_type'] = array(
    'bundle' => 'uw_ct_logos',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choose the type of logo.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'i18n_taxonomy',
        'settings' => array(),
        'type' => 'i18n_taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_logos_type',
    'label' => 'Type of logo',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-uw_para_logo-field_uw_logos_file'.
  $field_instances['paragraphs_item-uw_para_logo-field_uw_logos_file'] = array(
    'bundle' => 'uw_para_logo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_url_plain',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_uw_logos_file',
    'label' => 'File',
    'required' => 0,
    'settings' => array(
      'description_field' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => '',
      'file_extensions' => 'zip',
      'max_filesize' => '25 MB',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'assetbank' => 0,
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__banner-750w' => 0,
          'colorbox__banner-wide' => 0,
          'colorbox__body-500px-wide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__galleryformatter_slide' => 0,
          'colorbox__galleryformatter_thumb' => 0,
          'colorbox__image_gallery_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__image_gallery_standard' => 0,
          'colorbox__image_gallery_wide' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_banner-wide' => 0,
          'image_body-500px-wide' => 0,
          'image_focal_point_preview' => 0,
          'image_galleryformatter_slide' => 0,
          'image_galleryformatter_thumb' => 0,
          'image_image_gallery_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_image_gallery_standard' => 0,
          'image_image_gallery_wide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'label_help_description' => '',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-uw_para_logo-field_uw_logos_logo'.
  $field_instances['paragraphs_item-uw_para_logo-field_uw_logos_logo'] = array(
    'bundle' => 'uw_para_logo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_uw_logos_logo',
    'label' => 'Logo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'alt_field_required' => 1,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => '',
      'file_extensions' => 'png jpg',
      'focal_point' => 0,
      'focal_point_preview' => 0,
      'focal_point_styles' => array(
        'image_gallery_small' => 0,
        'image_gallery_squares' => 0,
        'image_gallery_standard' => 0,
        'image_gallery_wide' => 0,
      ),
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '2 MB',
      'max_resolution' => '400x400',
      'min_resolution' => '20x20',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__banner-750w' => 0,
          'colorbox__banner-wide' => 0,
          'colorbox__body-500px-wide' => 0,
          'colorbox__field-slideshow-slide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__image_gallery_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__image_gallery_standard' => 0,
          'colorbox__image_gallery_wide' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__uw_service_icon' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_banner-wide' => 0,
          'image_body-500px-wide' => 0,
          'image_field-slideshow-slide' => 0,
          'image_focal_point_preview' => 0,
          'image_image_gallery_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_image_gallery_standard' => 0,
          'image_image_gallery_wide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_uw_service_icon' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'label_help_description' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-uw_para_logo-field_uw_logos_logo_description'.
  $field_instances['paragraphs_item-uw_para_logo-field_uw_logos_logo_description'] = array(
    'bundle' => 'uw_para_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_plain',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_uw_logos_logo_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 'single_page_remote_events',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'label_help_description' => '',
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-uw_para_logo-field_uw_logos_use_black'.
  $field_instances['paragraphs_item-uw_para_logo-field_uw_logos_use_black'] = array(
    'bundle' => 'uw_para_logo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_uw_logos_use_black',
    'label' => 'Use black background',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
        'label_help_description' => '',
      ),
      'type' => 'options_onoff',
      'weight' => 1,
    ),
    'workbench_access_field' => 0,
  );

  // Exported field_instance: 'taxonomy_term-uw_tax_logos_types-field_image'.
  $field_instances['taxonomy_term-uw_tax_logos_types-field_image'] = array(
    'bundle' => 'uw_tax_logos_types',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_image',
    'label' => 'Choose an image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'alt_field_required' => 0,
      'default_image' => 0,
      'entity_translation_sync' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'focal_point' => 0,
      'focal_point_preview' => 0,
      'focal_point_styles' => array(
        'image_gallery_small' => 0,
        'image_gallery_squares' => 0,
        'image_gallery_standard' => 0,
        'image_gallery_wide' => 0,
      ),
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '10 MB',
      'max_resolution' => '3000x3000',
      'min_resolution' => '10x10',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 1,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'image',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__banner-750w' => 0,
          'colorbox__banner-wide' => 0,
          'colorbox__body-500px-wide' => 0,
          'colorbox__field-slideshow-slide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__image_gallery_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__image_gallery_standard' => 0,
          'colorbox__image_gallery_wide' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__uw_service_icon' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_banner-wide' => 0,
          'image_body-500px-wide' => 'image_body-500px-wide',
          'image_field-slideshow-slide' => 0,
          'image_focal_point_preview' => 0,
          'image_image_gallery_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_image_gallery_standard' => 0,
          'image_image_gallery_wide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 'image_sidebar-220px-wide',
          'image_thumbnail' => 0,
          'image_uw_service_icon' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => 500,
        'label_help_description' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-uw_tax_logos_types-field_uw_logos_requires_faculty'.
  $field_instances['taxonomy_term-uw_tax_logos_types-field_uw_logos_requires_faculty'] = array(
    'bundle' => 'uw_tax_logos_types',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Select if this type of logo requires a faculty to be chosen.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_uw_logos_requires_faculty',
    'label' => 'Requires faculty',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
        'label_help_description' => '',
      ),
      'type' => 'options_onoff',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-uw_tax_logos_types-field_uw_logos_show_faculty'.
  $field_instances['taxonomy_term-uw_tax_logos_types-field_uw_logos_show_faculty'] = array(
    'bundle' => 'uw_tax_logos_types',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Select if this logo is not to show the faculty on the logos page.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_uw_logos_show_faculty',
    'label' => 'Do not show faculty',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
        'label_help_description' => '',
      ),
      'type' => 'options_onoff',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Choose an image');
  t('Choose the faculty for this logo.');
  t('Choose the type of logo.');
  t('Description');
  t('Do not show faculty');
  t('Enter the descriptive text that will appear on the page that shows this logo.');
  t('Enter the link that the site footer logo will be directed to.   This is REQUIRED if you enter a logo.');
  t('Faculty');
  t('File');
  t('Link');
  t('Logo');
  t('Logos');
  t('Requires faculty');
  t('Select if this logo is not to show the faculty on the logos page.');
  t('Select if this type of logo requires a faculty to be chosen.');
  t('Select the logo to be used as the site footer on WCMS sites.');
  t('Type of logo');
  t('Use black background');

  return $field_instances;
}
