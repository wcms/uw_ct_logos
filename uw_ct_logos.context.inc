<?php

/**
 * @file
 * uw_ct_logos.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_logos_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_logo';
  $context->description = 'Displays logo filter for logos';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'uw-logos' => 'uw-logos',
        'uw-logos/*' => 'uw-logos/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_logos-logo_type_description' => array(
          'module' => 'uw_ct_logos',
          'delta' => 'logo_type_description',
          'region' => 'content',
          'weight' => '-10',
        ),
        'uw_ct_logos-logo_filters' => array(
          'module' => 'uw_ct_logos',
          'delta' => 'logo_filters',
          'region' => 'content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays logo filter for logos');
  $export['uw_logo'] = $context;

  return $export;
}
