<?php

/**
 * @file
 * uw_ct_logos.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_logos_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_uw-logos-settings:admin/config/system/uw_logos_settings.
  $menu_links['menu-site-management_uw-logos-settings:admin/config/system/uw_logos_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_logos_settings',
    'router_path' => 'admin/config/system/uw_logos_settings',
    'link_title' => 'UW logos settings',
    'options' => array(
      'attributes' => array(
        'title' => 'Setting page for UW logos.',
      ),
      'identifier' => 'menu-site-management_uw-logos-settings:admin/config/system/uw_logos_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('UW logos settings');

  return $menu_links;
}
