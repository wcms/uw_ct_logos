<?php

/**
 * @file
 * uw_ct_logos.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_logos_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer uw logos'.
  $permissions['administer uw logos'] = array(
    'name' => 'administer uw logos',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_logos',
  );

  // Exported permission: 'create uw_ct_logos content'.
  $permissions['create uw_ct_logos content'] = array(
    'name' => 'create uw_ct_logos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_logos content'.
  $permissions['delete any uw_ct_logos content'] = array(
    'name' => 'delete any uw_ct_logos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_logos content'.
  $permissions['delete own uw_ct_logos content'] = array(
    'name' => 'delete own uw_ct_logos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_tax_logos_faculties'.
  $permissions['delete terms in uw_tax_logos_faculties'] = array(
    'name' => 'delete terms in uw_tax_logos_faculties',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_tax_logos_types'.
  $permissions['delete terms in uw_tax_logos_types'] = array(
    'name' => 'delete terms in uw_tax_logos_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_logos content'.
  $permissions['edit any uw_ct_logos content'] = array(
    'name' => 'edit any uw_ct_logos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_logos content'.
  $permissions['edit own uw_ct_logos content'] = array(
    'name' => 'edit own uw_ct_logos content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_tax_logos_faculties'.
  $permissions['edit terms in uw_tax_logos_faculties'] = array(
    'name' => 'edit terms in uw_tax_logos_faculties',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_tax_logos_types'.
  $permissions['edit terms in uw_tax_logos_types'] = array(
    'name' => 'edit terms in uw_tax_logos_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_logos revision log entry'.
  $permissions['enter uw_ct_logos revision log entry'] = array(
    'name' => 'enter uw_ct_logos revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos authored by option'.
  $permissions['override uw_ct_logos authored by option'] = array(
    'name' => 'override uw_ct_logos authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos authored on option'.
  $permissions['override uw_ct_logos authored on option'] = array(
    'name' => 'override uw_ct_logos authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos comment setting option'.
  $permissions['override uw_ct_logos comment setting option'] = array(
    'name' => 'override uw_ct_logos comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos promote to front page option'.
  $permissions['override uw_ct_logos promote to front page option'] = array(
    'name' => 'override uw_ct_logos promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos published option'.
  $permissions['override uw_ct_logos published option'] = array(
    'name' => 'override uw_ct_logos published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos revision option'.
  $permissions['override uw_ct_logos revision option'] = array(
    'name' => 'override uw_ct_logos revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_logos sticky option'.
  $permissions['override uw_ct_logos sticky option'] = array(
    'name' => 'override uw_ct_logos sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
