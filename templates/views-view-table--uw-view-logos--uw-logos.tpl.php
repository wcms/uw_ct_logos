<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 *
 * @ingroup views_templates
 */
?>

<?php $current_row = ''; $current_faculty = ''; ?>

<?php foreach ($rows as $row_count => $row): ?>

  <?php
    if ($current_row !== $row['title']) {
      $current_row = $row['title'];
  ?>
    <div class="uw-logos-title-faculty-details">
      <div class="uw-logos-title-faculty">
        <h3><?php print $row['title']; ?></h3>

        <?php
          $term = NULL;

          if (isset($row['field_uw_logos_faculty']) && $row['field_uw_logos_faculty'] !== '') {
            $term = current(taxonomy_get_term_by_name($row['field_uw_logos_type']));
            $entity = entity_metadata_wrapper('taxonomy_term', $term);
          }
        ?>

        <?php if ($term !== NULL && isset($entity->field_uw_logos_show_faculty) && !$entity->field_uw_logos_show_faculty->value()): ?>
          <p>Faculty of <?php print $row['field_uw_logos_faculty']; ?></p>
        <?php endif; ?>
      </div>
      <div class="uw-logos-details">
        <?php print $row['view_node']; ?>
      </div>
    </div>
    <table class="uw-logos-logos"<?php print $attributes; ?>>
      <tr>
        <td class="uw-logos-logo">
          <table class="uw-logos-logo-header">
            <thead>
            <tr>
              <th class="one-half">Logo example</th>
              <th class="one-quarter">Description</th>
              <th class="one-quarter">Download link</th>
            </tr>
            </thead>
          </table>
        </td>
      </tr>
    </table>
  <?php } ?>

  <table class="uw-logos-logos"<?php print $attributes; ?>>

    <tbody>

      <tr>
        <td class="uw-logos-logo">
          <table>
            <tr>

              <?php
              $classes = 'col1';

              if (isset($row['field_uw_logos_use_black'])) {
                $classes .= ' ' . $row['field_uw_logos_use_black'];
              }

              if (isset($row['field_uw_logos_site_footer'])) {
                $classes .= ' ' . $row['field_uw_logos_site_footer'];
              }
              ?>

              <td class="<?php print $classes; ?>">
                <?php print $row['field_uw_logos_logo']; ?>
              </td>

              <td class="col2">
                <?php print $row['field_uw_logos_logo_description']; ?>
              </td>

              <td class="col3">
                <?php
                if (isset($row['field_uw_logos_file']) && $row['field_uw_logos_file'] !== NULL) {
                  print '<a href="' . $row['field_uw_logos_file'] . '">Download logo kit (zip)</a>';
                }
                ?>
              </td>

            </tr>
          </table>
        </td>
      </tr>

    </tbody>
  </table>


<?php endforeach; ?>

