<div class="uw-logos-wrapper">

  <?php if (isset($content['description']) && $content['description'] !== ''): ?>
    <div class="uw-logos-description">
      <?php print $content['description']; ?>
    </div>
  <?php endif; ?>

  <?php if (variable_get('uw_logos_node_message') !== ''): ?>
    <div class="uw-logos-downloads">
      <?php print variable_get('uw_logos_node_message')['value']; ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['table'])) { ?>
    <div class="uw-logos-logos">
      <?php print render($content['table']); ?>
    </div>
  <?php } ?>

  <?php if (isset($content['field_uw_logos_sf_logo'])): ?>
    <h2>Site Footer</h2>
    <p>
      This is the logo that is used as the site footer logo in WCMS sites.
    </p>
    <div class="uw-logos-site-footer">
      <?php print render($content['field_uw_logos_sf_logo']); ?>
    </div>
  <?php endif; ?>
</div>
