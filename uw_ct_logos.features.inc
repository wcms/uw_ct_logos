<?php

/**
 * @file
 * uw_ct_logos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_logos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_logos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_logos_node_info() {
  $items = array(
    'uw_ct_logos' => array(
      'name' => t('UW Logos'),
      'base' => 'node_content',
      'description' => t('Logos for the University of Waterloo'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_logos_paragraphs_info() {
  $items = array(
    'uw_para_logo' => array(
      'name' => 'UW logo',
      'bundle' => 'uw_para_logo',
      'locked' => '1',
    ),
  );
  return $items;
}
