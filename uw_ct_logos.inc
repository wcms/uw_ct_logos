<?php

/**
 * Helper function.
 *
 * The types of logos to create on install.
 */
function _uw_ct_logos_get_types_of_logos() {

  $types[] = array(
    'name' => 'University',
    'description' => 'A University of Waterloo logo.',
    'weight' => 0,
  );

  $types[] = array(
    'name' => 'Faculty',
    'description' => 'Faculty',
    'weight' => 1,
  );

  $types[] = array(
    'name' => 'School',
    'description' => 'School',
    'weight' => 1,
  );

  $types[] = array(
    'name' => 'Academic department and unit',
    'description' => 'A academic department of unit.',
    'weight' => 2,
  );

  $types[] = array(
    'name' => 'Non-academic department and unit',
    'description' => 'Non-academic department and unit.',
    'weight' => 3,
  );

  $types[] = array(
    'name' => 'Co-brands, partnerships and sponsorship',
    'description' => 'Co-brands, partnerships and sponsorship.',
    'weight' => 4,
  );

  $types[] = array(
    'name' => 'Event',
    'description' => 'Events.',
    'weight' => 5,
  );

  $types[] = array(
    'name' => 'Swag',
    'description' => 'Swag.',
    'weight' => 6,
  );

  return $types;
}

/**
 * Helper function.
 *
 * The faculties for UW logos to create on install.
 */
function _uw_ct_logos_get_faculties() {

  $faculties[] = array(
    'name' => 'University of Waterloo',
    'description' => 'University of Waterloo.',
    'weight' => 0,
  );

  $faculties[] = array(
    'name' => 'Applied Health Sciences',
    'description' => 'Applied Health Sciences.',
    'weight' => 1,
  );

  $faculties[] = array(
    'name' => 'Arts',
    'description' => 'Arts.',
    'weight' => 2,
  );

  $faculties[] = array(
    'name' => 'Engineering',
    'description' => 'Engineering.',
    'weight' => 3,
  );

  $faculties[] = array(
    'name' => 'Environment',
    'description' => 'Environment.',
    'weight' => 4,
  );

  $faculties[] = array(
    'name' => 'Mathematics',
    'description' => 'Mathematics.',
    'weight' => 5,
  );

  $faculties[] = array(
    'name' => 'Science',
    'description' => 'Science.',
    'weight' => 6,
  );

  return $faculties;
}
